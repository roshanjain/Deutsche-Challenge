package com.db.awmd.challenge;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.model.Response;
import com.db.awmd.challenge.model.TransferFundRequest;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.service.TransactionService;


@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class TransactionControllerTest {

    private MockMvc mockMvc;
    
    @Autowired
    private AccountsService accountsService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void prepareMockMvc() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
        transactionService= Mockito.mock(TransactionService.class);
    }

    @Test
    public void transferFundSuccessTest() throws Exception {
        createAccount();
        Mockito.when(transactionService.transferFund(Mockito.any(TransferFundRequest.class))).thenReturn(transferFundResponse());
        this.mockMvc.perform(post("/v1/transferfund").contentType(MediaType.APPLICATION_JSON)
                .content("{\"fromAcId\":4,\"toAcId\":1,\"amount\":100}")).andExpect(status().isOk());
        accountsService.getAccountsRepository().clearAccounts();
    }

    @Test(expected = NestedServletException.class)
    public void accountNotFoundTest() throws Exception {
        createAccount();
        Mockito.when(transactionService.transferFund(Mockito.any(TransferFundRequest.class))).thenReturn(transferFundResponse());
        this.mockMvc.perform(post("/v1/transferfund").contentType(MediaType.APPLICATION_JSON)
                .content("{\"fromAcId\":895,\"toAcId\":1,\"amount\":100}")).andExpect(status().isExpectationFailed());
        accountsService.getAccountsRepository().clearAccounts();
    }

    @Test
    public void negativeFundTransferTest() throws Exception {
    	accountsService.getAccountsRepository().clearAccounts();
        createAccount();
        Mockito.when(transactionService.transferFund(Mockito.any(TransferFundRequest.class))).thenReturn(transferFundResponse());
        this.mockMvc.perform(post("/v1/transferfund").contentType(MediaType.APPLICATION_JSON)
                .content("{\"fromAcId\":2,\"toAcId\":1,\"amount\":-100}")).andExpect(status().isBadRequest());
        accountsService.getAccountsRepository().clearAccounts();
    }
    @Test
    public void sameAccountTransferTest() throws Exception {
    	accountsService.getAccountsRepository().clearAccounts();
        createAccount();
        Mockito.when(transactionService.transferFund(Mockito.any(TransferFundRequest.class))).thenReturn(transferFundResponse());
        this.mockMvc.perform(post("/v1/transferfund").contentType(MediaType.APPLICATION_JSON)
                .content("{\"fromAcId\":1,\"toAcId\":1,\"amount\":-100}")).andExpect(status().isBadRequest());
        accountsService.getAccountsRepository().clearAccounts();
    }
    private Response transferFundResponse(){
    	Response response=new Response();
        response.setMessage("success");
        response.setResponseCode(200);
        return response;
    }
    private void  createAccount(){
        Account account = new Account("4");
        account.deposit(new BigDecimal(1000));
        this.accountsService.createAccount(account);
        Account account1 = new Account("1");
        account.deposit(new BigDecimal(1000));
        this.accountsService.createAccount(account1);
    }
}
