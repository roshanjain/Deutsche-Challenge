package com.db.awmd.challenge.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
/**
 * Rest Api Request POJO to handle amount transfer from one account to another account
 * 
 * @author Roshan Jain
 * @since Mar 14, 2018
 */
@Setter
@Getter
public class TransferFundRequest {

    @NotBlank(message = "Account from Id must not be empty")
    @JsonProperty(value = "fromAcId")
    private String fromAccountId;

	@NotBlank(message = "Account to Id must not be empty")
    @JsonProperty(value = "toAcId")
    private String toAccountId;

    @JsonProperty(value = "amount")
    @DecimalMin("1")
    private BigDecimal amount;
    
    public String getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public String getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
    
}