package com.db.awmd.challenge.model;

/**
 * Use for the response 
 * @author Roshan Jain
 * @since Mar 14, 2018
 */
public class Response {
	private Integer responseCode;
    private String message;

    public Response(){
    }
    
    public Response(Integer responseCode, String message){
    	this.responseCode = responseCode;
    	this.message = message;
    }
    
    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
