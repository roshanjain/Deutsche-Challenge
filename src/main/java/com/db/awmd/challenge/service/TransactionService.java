package com.db.awmd.challenge.service;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountNotFoundException;
import com.db.awmd.challenge.exception.InsufficientBalanceException;
import com.db.awmd.challenge.model.Response;
import com.db.awmd.challenge.model.TransferFundRequest;
import com.db.awmd.challenge.repository.AccountsRepository;
import com.db.awmd.challenge.util.ResponseBuilder;

/**
 * Transaction service to handle the account transaction
 * currently fund transfer is implemented 
 * 
 * @author Roshan Jain
 * @since Mar 14, 2018
 */
@Service
public class TransactionService {

	@Getter
	private final AccountsRepository accountsRepository;

	@Getter
	private final NotificationService notificationService;

	@Autowired
	public TransactionService(AccountsRepository accountsRepository,
			NotificationService notificationService) {
		this.accountsRepository = accountsRepository;
		this.notificationService = notificationService;
	}
	
	/**
	 * @param accountId
	 * @return
	 */
	private Account getAccount(String accountId) {
		Account account = this.accountsRepository.getAccount(accountId);
		if (account == null) {
			throw new AccountNotFoundException(accountId);
		}
		return account;
	}

	/**
	 * @param transferRequest
	 * @return
	 * @throws InterruptedException 
	 * @throws InsufficientBalanceException 
	 */
	public Response transferFund(TransferFundRequest transferRequest) throws InsufficientBalanceException, InterruptedException {
			Account fromAccount = getAccount(transferRequest.getFromAccountId());
			Account toAccount = getAccount(transferRequest.getToAccountId());
			BigDecimal amount = transferRequest.getAmount();
			makeTransferFundTransaction(fromAccount, toAccount, amount);
			sendEmailNotification(fromAccount, toAccount, amount);
		return ResponseBuilder.buildResponse(200,
					"Fund Successfully Transfer");
		
	}

	/**
	 * @param fromAccount
	 * @param toAccount
	 * @param amount
	 * @throws InsufficientBalanceException
	 * @throws InterruptedException
	 */
	private void makeTransferFundTransaction(Account fromAccount,
			Account toAccount, BigDecimal amount) throws InsufficientBalanceException, InterruptedException {
		
		if (fromAccount.lock.tryLock(100, TimeUnit.MILLISECONDS))
	    {
	      try {
	        if (toAccount.lock.tryLock(100, TimeUnit.MILLISECONDS))
	        {
	          try {
	        	  fromAccount.withdraw(amount);
	        	  toAccount.deposit(amount);
	          }
	          finally {
	        	  toAccount.lock.unlock();
	          }
	        }
	      }
	      finally {
	    	  fromAccount.lock.unlock();
	      }
	    }
		
	}

	/**
	 * @param fromAccount
	 * @param toAccount
	 * @param amount
	 */
	private void sendEmailNotification(Account fromAccount, Account toAccount,
			BigDecimal amount) {
		
		CompletableFuture.runAsync(() -> {
			notificationService.notifyAboutTransfer(
					fromAccount,
					"An amount of Rs. " + amount
							+ " has been debited from your account "
							+ fromAccount.getAccountId());
        });

		
		CompletableFuture.runAsync(() -> {
		notificationService.notifyAboutTransfer(
				toAccount,
				"An amount of Rs. " + amount
						+ " has been credited to your account "
						+ toAccount.getAccountId());
		 });
	}
}
