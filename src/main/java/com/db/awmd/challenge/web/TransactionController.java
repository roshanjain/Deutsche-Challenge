package com.db.awmd.challenge.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.db.awmd.challenge.exception.InsufficientBalanceException;
import com.db.awmd.challenge.model.Response;
import com.db.awmd.challenge.model.TransferFundRequest;
import com.db.awmd.challenge.service.NotificationService;
import com.db.awmd.challenge.service.TransactionService;
import com.db.awmd.challenge.util.ResponseBuilder;

@RestController
@RequestMapping("/v1/")
public class TransactionController {

	private final TransactionService transactionService;

	private final Logger log = LoggerFactory
			.getLogger(TransactionController.class);

	@Autowired
	public TransactionController(TransactionService transactionService,
			NotificationService notificationService) {
		this.transactionService = transactionService;
	}

	/**
	 * End point for the transfer fund from one account to another
	 * From and to account should not be same
	 * Transfer amount value should be grater the current balance of from account
	 * 
	 * @param transferFundRequest
	 * @return
	 * @throws InsufficientBalanceException
	 */
	@PostMapping(value = "/transferfund", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> transferFund(
			@RequestBody @Valid TransferFundRequest transferFundRequest)
			throws InsufficientBalanceException {
		log.info("Entring into TransactionController :: transferFund");
		if (transferFundRequest.getFromAccountId().equals(
				transferFundRequest.getToAccountId())) {
			return new ResponseEntity<>(ResponseBuilder.buildResponse(400,
					"Acount From and To Should not be same"),
					HttpStatus.BAD_REQUEST);
		}
		try {
			return ResponseEntity.ok(this.transactionService
					.transferFund(transferFundRequest));
		} catch (InterruptedException e) {
			return new ResponseEntity<>(ResponseBuilder.buildResponse(400,
					"Sorry, due to some technical issue we are not process your request now. Please try after some time."),
					HttpStatus.EXPECTATION_FAILED);
		}

	}

}
