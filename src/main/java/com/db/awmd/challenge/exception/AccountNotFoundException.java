package com.db.awmd.challenge.exception;

/**
 * This exception is defined for get account action: When user try to get account details and account is 
 * available than this exception will be occur
 * 
 * @author Roshan Jain
 * @since Mar 14, 2018
 */
public class AccountNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 255721644032051440L;

	public AccountNotFoundException(String accountId) {
        super("Account " + accountId + " Is Not Found!");
    }
}
