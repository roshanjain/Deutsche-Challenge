package com.db.awmd.challenge.exception;

/**
 * This exception is defined for withdraw action: When user try to withdraw the
 * amount more than current balance then this exception will be occur
 * 
 * @author Roshan Jain
 * @since Mar 14, 2018
 */
public class InsufficientBalanceException extends RuntimeException {

	private static final long serialVersionUID = -4535087045857911653L;

	public InsufficientBalanceException(String message) {
		super(message);
	}
}
