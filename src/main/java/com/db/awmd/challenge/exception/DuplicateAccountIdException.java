package com.db.awmd.challenge.exception;

/**
 * This exception throws when account id is duplicate.
 *  
 * @author Roshan Jain
 * @since Mar 21, 2018
 */
public class DuplicateAccountIdException extends RuntimeException {

	private static final long serialVersionUID = -609680503837641957L;

	public DuplicateAccountIdException(String message) {
		super(message);
	}
}
