package com.db.awmd.challenge.domain;

import com.db.awmd.challenge.exception.InsufficientBalanceException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Data
public class Account {

	@NotNull
	@NotEmpty
	private final String accountId;

	@NotNull
	@Min(value = 0, message = "Initial balance must be positive.")
	private BigDecimal balance;
	
	public final Lock lock = new ReentrantLock();

	public Account(String accountId) {
		this.accountId = accountId;
		this.balance = BigDecimal.ZERO;
	}

	@JsonCreator
	public Account(@JsonProperty("accountId") String accountId,
			@JsonProperty("balance") BigDecimal balance) {
		this.accountId = accountId;
		this.balance = balance;
	}

	public String getAccountId() {
		return accountId;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	/**
	 * Deposit amount into account. If deposit amount is less than zero
	 * then throws illegal argument exception.
	 * 
	 * @param amount
	 */
	public void deposit(BigDecimal amount) {
		this.balance = this.balance.add(amount);
	}

	/**
	 * Withdraw amount from account. If withdraw amount is less than current
	 * balance then throws insufficient balance exception.
	 * 
	 * @param amount
	 * @throws InsufficientBalanceException
	 */
	public void withdraw(BigDecimal amount) throws InsufficientBalanceException {
		// Verifying overdrawn
		if (this.balance.compareTo(amount) < 0) {
			throw new InsufficientBalanceException("Insufficient Balance:: "
					+ this.balance + " In Account:: " + this.accountId);
		}
		this.balance = this.balance.subtract(amount);
	}
}
