package com.db.awmd.challenge.util;

import com.db.awmd.challenge.model.Response;

public class ResponseBuilder {
	
	public static Response buildResponse(Integer code, String message){
		return new Response(code, message);
	}
}
